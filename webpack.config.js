// webpack.config.js
const webpack = require('webpack')
const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    desktop_homepage: './scss/pages/desktop/desktop_homepage.scss',
    desktop_hotels: './scss/pages/desktop/desktop_hotel.scss',
    desktop_hotels2: './scss/pages/desktop/desktop_hotel2.scss',
    desktop_serp: './scss/pages/desktop/desktop_serp.scss',
    desktop_fod: './scss/pages/desktop/desktop_fod.scss',
    desktop_booking: './scss/pages/desktop/desktop_booking.scss',
    call_back: './scss/pages/desktop/call_back.scss',
    mobile_hotel: './scss/pages/mobile/mobile_hotel.scss',
    test_module: './scss/pages/desktop/test_module.scss',
    slider_calendar: './scss/pages/desktop/slider_calendar.scss',
    desktop_ai: './scss/pages/desktop/desktop_ai.scss',
    desktop_ai_v2: './scss/pages/desktop/desktop_ai_v2.scss',
    grid_system: './scss/pages/desktop/grid_system.scss',
    mobile_homepage: './scss/pages/mobile/mobile_homepage.scss',
    homepage_critical: './scss/pages/desktop/homepage_critical.scss',
    homepage_remain: './scss/pages/desktop/homepage_remain.scss',
    desktop_guides: './scss/pages/desktop/desktop_guides.scss',
    desktop_fodrich: './scss/pages/desktop/desktop_fodrich.scss',
    fonts: './scss/pages/desktop/fonts.scss',
    footer_for_old_pages: './scss/pages/desktop/footer_for_old_pages.scss',
    desktop_ie_homepage: './scss/pages/ireland/desktop_ie_homepage.scss',
    search: './scss/pages/desktop/search.scss', // Search component
    desktop_complaint_form: './scss/pages/desktop/desktop_complaint_form.scss',

    /* Old Files
    desktop_ai_old: './scss/pages/desktop/desktop_ai_old.scss', // old all inclusive file
    desktop_ch: './scss/pages/desktop/desktop_ch.scss', // cheap holidays
    desktop_gh: './scss/pages/desktop/desktop_gh.scss', // guides home
    desktop_gd: './scss/pages/desktop/desktop_gd.scss', // guides details
    desktop_hh: './scss/pages/desktop/desktop_hh.scss', // help home
    desktop_hm: './scss/pages/desktop/desktop_hm.scss', // holidays main
    desktop_mh: './scss/pages/desktop/desktop_mh.scss', // my holidays
    desktop_theme: './scss/pages/desktop/desktop_theme.scss', // old theme file
    desktop_ph: './scss/pages/desktop/desktop_ph.scss', // pagelevel-holidays
    search: './scss/pages/desktop/search.scss',  */ // search



    //base: './scss/pages/base.scss',
    //hiddenelements: './scss/pages/hiddenelements.scss'asdf,
    //homepage: './scss/pages/homepage.scss',
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js'
  },
  module: {
    rules: [{
      test: /\.js$/,
      include: path.resolve(__dirname, 'src'),
      use: [{
        loader: 'babel-loader',
        options: {
          presets: [
            ['es2015', {
              modules: false
            }]
          ]
        }
      }]
    }, { // sass / scss loader for webpack
      test: /\.(sass|scss)$/,
      loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
    }]
  },

  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css',
    })
  ]
}

module.exports = config
