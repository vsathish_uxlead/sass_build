
var jsonData;
var pound = 'Â£';
var pound = jQuery()
jQuery.ajax({
  url: "http://dataservice.teletextholidays.co.uk/GetJsonService.svc/GetSectionObjects?sectionIds=Last_Minute_TopDestinations,Last_Minute_Deals_Box1,Last_Minute_Deals_Box2,Last_Minute_Deals_Box3,Last_Minute_Deals_Box4,Last_Minute_Deals_Box5,Last_Minute_Deals_Box6,Last_Minute_Deals_Box7,Last_Minute_Deals_Box8,Last_minute_hotels",
  method: 'Get',
  dataType: 'json',
  success:function(data,status,jqxhr) {
    jsonData = data;
    var tags = 0;
    var offercounter = 0;
    jQuery.each(jsonData, function(index,value) {
      var cheapHotelOffers = jQuery('#cheapHolidaysOffersTemplate').html();

      var AiDealItem = jQuery('#aiDealsTemplate').html();
      var cheapHotelOffersDiv = document.createElement('div');
      var AiDealItemDiv = document.createElement('a');
      if(index < 3 ) {

        /* First Carousel */
        jQuery(AiDealItemDiv).html(AiDealItem);
        jQuery(AiDealItemDiv).attr('href', value.query.websiteUrl);
        jQuery(AiDealItemDiv).find('.deal-img img').attr("data-src", value.image);
        jQuery(AiDealItemDiv).find('.deal-img img').attr('alt', value.query.name);
        jQuery(AiDealItemDiv).find('.deal-img img').attr('title', value.query.name);
        jQuery(AiDealItemDiv).find('.ai-holidays-title').text(value.query.description);
        jQuery(AiDealItemDiv).find('.tth-price').html('&pound' + value.minPrice);
        jQuery('#aiDealsWrapper').append(AiDealItemDiv);

        /* Cheap Holidays */
        var tagNames = document.createElement('span');
        if(tags == 0) {
          tagNames.className = 'tag-name bgnd-purple txt-white';
        }else if (tags == 1) {
          tagNames.className = 'tag-name bgnd-cornflower-blue txt-white';
        }else if(tags == 2) {
          tagNames.className = 'tag-name bgnd-sunset txt-white';
        }else {
          tagNames.className = 'tag-name bgnd-red-light txt-white';
        }

        /* Images */
        cheapHotelOffersDiv.className = 'cheap-hotel-offer deal-thin-item pstn-reltv';
        jQuery(cheapHotelOffersDiv).html(cheapHotelOffers);
        jQuery(cheapHotelOffersDiv).find('.cheap-hotel-img img').attr("data-src", value.image);
        jQuery(cheapHotelOffersDiv).find('.cheap-hotel-img img').attr('alt', value.query.name);
        jQuery(cheapHotelOffersDiv).find('.cheap-hotel-img img').attr('title', value.query.name);
        jQuery(cheapHotelOffersDiv).find('.deal-tag').append(tagNames);
        jQuery(cheapHotelOffersDiv).find('.deal-tag span').text(value.query.description);

        /* Price Row
        jQuery.each(jsonData, function(priceInfo, infoValue){
          if(priceInfo < 2 ) {
            var offerPriceRowTemplateDiv = jQuery('.offerPriceRowTemplate').html();
            var offerPriceRowDiv = document.createElement('div');
            offerPriceRowDiv.className = 'deal-price pstn-reltv grid-stay items-center space-between';
            jQuery(offerPriceRowDiv).html(offerPriceRowTemplateDiv);
            jQuery(offerPriceRowDiv).find('.dstn-name').text(infoValue.query.description);
            jQuery(cheapHotelOffersDiv).append(offerPriceRowDiv);
          }
        }); */

        /* Price Row */
        var s= 0;
        var sv =0;
        switch(offercounter){
          case 0:
          s=2;sv=0;
          break;
          case 1:s=4;sv=2;
          break;
          case 2:s=6;sv=4;
          break;
        }
        for(var i = sv; i < s; i++) {
          var offerPriceRowTemplateDiv = jQuery('.offerPriceRowTemplate').html();
          var offerPriceRowDiv = document.createElement('div');
          offerPriceRowDiv.className = 'deal-price pstn-reltv grid-stay items-center space-between';
          jQuery(offerPriceRowDiv).html(offerPriceRowTemplateDiv);
          jQuery(offerPriceRowDiv).find('.dstn-name').text(jsonData[i].query.description);
          jQuery(offerPriceRowDiv).find('.tth-price').text(jsonData[i].minPrice);
          jQuery(cheapHotelOffersDiv).append(offerPriceRowDiv);
        }
        offercounter += 1;

        jQuery('#cheapHolidaysWrapper').append(cheapHotelOffersDiv);




        tags++
        if(tags==4) {
          tags = 0;
        }


      }
      console.log(jsonData);
    })
  }, error(data) {
    alert();
  }
})
